package net.kotlincook.voting.gui

import com.vaadin.flow.component.dependency.StyleSheet
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.router.Route

@Route("")
@StyleSheet("frontend://landing.css")
class Landing : VerticalLayout()